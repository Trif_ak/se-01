package ru.trifonov.tm.entity.service.impl;

import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.entity.service.TaskService;
import java.util.ArrayList;

public class TaskServiceImpl implements TaskService {
    private ArrayList<Task> tasks = new ArrayList<>();

    @Override
    public void create(String name, int id, int idProject) {
        Task task = new Task();
        task.setName(name);
        task.setId(id);
        task.setIdProject(idProject);
        tasks.add(task);
    }

    @Override
    public void update(String name, int id) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getId() == id) tasks.get(i).setName(name);
        }
    }

    @Override
    public void read(int idProject) {
        for (Task task : tasks) {
            if (task.getIdProject() == idProject) System.out.println(task.getId() + ". " + task.getName());
        }
    }

    @Override
    public void delete(int id) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getId() == id) tasks.remove(i);
        }
    }

    @Override
    public void clear(int idProject) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getIdProject() == idProject) tasks.remove(i);
        }
    }

    /*Getters and Setters*/

    public ArrayList<Task> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<Task> tasks) {
        this.tasks = tasks;
    }
}
