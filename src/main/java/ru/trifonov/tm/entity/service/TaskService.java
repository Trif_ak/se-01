package ru.trifonov.tm.entity.service;

public interface TaskService {
    void create(String name, int id, int idProject);
    void update(String name, int id);
    void read(int idProject);
    void delete(int id);
    void clear(int idProject);
}
