package ru.trifonov.tm.entity.service.impl;

import ru.trifonov.tm.entity.service.ProjectService;
import ru.trifonov.tm.entity.Project;

import java.util.ArrayList;

public class ProjectServiceImpl implements ProjectService {
    private ArrayList<Project> projects = new ArrayList<>();

    /*CRUD methods*/

    @Override
    public void create(String name, int id) {
        Project project = new Project();
        project.setName(name);
        project.setId(id);
        projects.add(project);
    }

    @Override
    public void update(String name, int id) {
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getId() == id) projects.get(i).setName(name);
        }
    }

    @Override
    public void read() {
        for (Project project : projects) {
            System.out.println(project.getId() + ". " + project.getName());
        }
    }

    @Override
    public void delete(int id) {
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getId() == id) projects.remove(i);
        }
    }

    @Override
    public void clear() {
        projects.removeAll(projects);
    }

    /*Getters ans Setters*/

    public ArrayList<Project> getProjects() {
        return projects;
    }

    public void setProjects(ArrayList<Project> projects) {
        this.projects = projects;
    }
}
