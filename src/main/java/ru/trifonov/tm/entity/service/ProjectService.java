package ru.trifonov.tm.entity.service;

public interface ProjectService {
    void create(String name, int id);
    void update(String name, int id);
    void read();
    void delete(int id);
    void clear();
}
