package ru.trifonov.tm.entity.view;

import ru.trifonov.tm.entity.service.impl.ProjectServiceImpl;
import ru.trifonov.tm.entity.service.impl.TaskServiceImpl;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        int idProject = 1;
        int idTask = 1;
        ProjectServiceImpl projectService = new ProjectServiceImpl();
        TaskServiceImpl taskService = new TaskServiceImpl();
        Scanner inCommand = new Scanner(System.in);

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("Enter \"help\" for show all commands. ");

        String command;
        int projectId;
        int taskId;
        do {
            command = inCommand.nextLine();
            switch (command) {
                case ("h"):
                case ("help"):
                    System.out.println("help: Show all commands. " +
                            "\n project-create (pc): Create new project." +
                            "\n project-update (pu): Update selected project." +
                            "\n project-list (pl): Show all ru.trifonov.tm.entity." +
                            "\n project-remove (pr): Remove selected project." +
                            "\n project-clear (pcl): Remove all ru.trifonov.tm.entity." +
                            "\n" +
                            "\n task-create (tc): Create new task." +
                            "\n task-update (tu): Update selected project." +
                            "\n task-list (tl): Show all tasks." +
                            "\n task-remove (tr): Remove selected task." +
                            "\n task-clear (tcl): Remove all tasks." +
                            "\n " +
                            "\n exit: Exit from app.");
                    break;

                    /*Projects commands*/

                case ("pc"):
                case ("project-create"):
                    System.out.println("[PROJECT CREATE]");
                    System.out.println("Enter name");
                    String name = inCommand.nextLine();
                    projectService.create(name, idProject++);
                    System.out.println("[OK]");
                    break;
                case ("pu"):
                case ("project-update"):
                    System.out.println("PROJECT UPDATE");
                    System.out.println("Enter the ID of the project you want to update");
                    projectId = Integer.parseInt(inCommand.nextLine());
                    System.out.println("Enter new name");
                    name = inCommand.nextLine();
                    projectService.update(name, projectId);
                    System.out.println("[OK]");
                    break;
                case ("pl"):
                case ("project-list"):
                    System.out.println("[ALL PROJECT LIST]");
                    projectService.read();
                    System.out.println("[OK]");
                    break;
                case ("pr"):
                case ("project-remove"):
                    System.out.println("[PROJECT REMOVE]");
                    System.out.println("Enter the ID of the project you want to delete");
                    projectId = Integer.parseInt(inCommand.nextLine());
                    projectService.delete(projectId);
                    System.out.println("[OK]");
                    break;
                case ("pcl"):
                case ("project-clear"):
                    System.out.println("[ALL PROJECT CLEAR]");
                    projectService.clear();
                    idProject = 1;
                    System.out.println("[OK]");
                    break;

                    /*Task commands*/

                case ("tc"):
                case ("task-create"):
                    System.out.println("[TASK CREATE]");
                    System.out.println("Enter ID of project");
                    projectId = Integer.parseInt(inCommand.nextLine());
                    System.out.println("Enter name task");
                    command = inCommand.nextLine();
                    taskService.create(command, idTask++, projectId);
                    System.out.println("[OK]");
                    break;
                case ("tu"):
                case ("task-update"):
                    System.out.println("[TASK UPDATE]");
                    System.out.println("Enter the ID of the task you want to update");
                    taskId = Integer.parseInt(inCommand.nextLine());
                    System.out.println("Enter new name");
                    name = inCommand.nextLine();
                    taskService.update(name, taskId);
                    System.out.println("[OK]");
                    break;
                case ("tl"):
                case ("task-list"):
                    System.out.println("[TASK LIST]");
                    System.out.println("Enter ID of project");
                    projectId = Integer.parseInt(inCommand.nextLine());
                    taskService.read(projectId);
                    System.out.println("[OK]");
                    break;
                case ("tr"):
                case ("task-remove"):
                    System.out.println("[TASK CLEAR]");
                    System.out.println("Enter the ID of the task you want to delete");
                    taskId = Integer.parseInt(inCommand.nextLine());
                    taskService.delete(taskId);
                    System.out.println("[OK]");
                    break;
                case ("tcl"):
                case ("task-clear"):
                    System.out.println("[TASK REMOVE]");
                    System.out.println("Enter ID of project");
                    projectId = Integer.parseInt(inCommand.nextLine());
                    taskService.clear(projectId);
                    idTask = 1;
                    System.out.println("[OK]");
                    break;
            }
        } while (!command.equals("exit"));
    }
}